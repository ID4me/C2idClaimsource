#!groovy
pipeline {
	agent any
	options {
		buildDiscarder(logRotator(numToKeepStr: '24'))
		disableConcurrentBuilds()
	}
	environment {
		VERSION = sh (script: 'date --utc +%y%m%d.%H%M --date="@$(git show --no-patch --format=%ct)"', returnStdout: true).trim()
	}
	triggers {
		pollSCM('TZ=Europe/Berlin\n' +
					'H/5 8-18 * * 1-5')
	}
	stages {
		stage('Checkout') {
			steps {
				checkout([$class: 'GitSCM',
							branches: [[name: '*/master']],
							doGenerateSubmoduleConfigurations: false,
							extensions: [],
							submoduleCfg: [],
							userRemoteConfigs: [[credentialsId: 'domainid-bot-ssh-key', url: 'git@gitlab.com:ID4me/C2idClaimsource.git']]])
			}
		}

		stage('Build') {
			steps {
				timestamps {
					sh './gradlew -Pversion=${VERSION} --no-daemon clean build javadoc projectReport'
				}
			}
			post {
				always {
					junit 'build/test-results/test/*.xml'
				}
			}
		}

		stage('Archiving artifacts and publishing reports') {
			steps {
				archiveArtifacts artifacts: 'build/libs/*.*, build/3rdPartyLibs/*.*', onlyIfSuccessful: true
				jacoco classPattern: 'build/classes/java', execPattern: 'build/jacoco/*.exec', sourcePattern: 'src/*/java'
				findbugs canComputeNew: false, defaultEncoding: 'ISO-8859-1', excludePattern: '', healthy: '0', includePattern: '', pattern: 'build/reports/findbugs/*.xml', unHealthy: '3'
				pmd canComputeNew: false, defaultEncoding: 'ISO-8859-1', healthy: '0', pattern: 'build/reports/pmd/*.xml', unHealthy: '3'
				publishHTML([allowMissing: true,
						alwaysLinkToLastBuild: false,
						keepAll: false,
						reportDir: 'build/docs/javadoc',
						reportFiles: 'index.html',
						reportName: 'JavaDoc',
						reportTitles: 'JavaDoc'])
			}
		}
	}
}

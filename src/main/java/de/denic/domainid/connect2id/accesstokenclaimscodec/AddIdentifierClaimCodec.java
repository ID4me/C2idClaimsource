/*^
  ===========================================================================
  ID4me Connect2ID ClaimSource
  ===========================================================================
  Copyright (C) 2017-2018 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.domainid.connect2id.accesstokenclaimscodec;

import com.nimbusds.jwt.JWTClaimsSet;
import com.nimbusds.oauth2.sdk.Scope;
import com.nimbusds.oauth2.sdk.id.ClientID;
import com.nimbusds.openid.connect.provider.spi.tokens.*;
import net.jcip.annotations.Immutable;
import net.minidev.json.JSONObject;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.text.ParseException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import static java.util.Collections.emptySet;

/**
 * Hint: If renaming this type, update content of Java {@link java.util.ServiceLoader} config file appropriately!
 */
@Immutable
public class AddIdentifierClaimCodec extends BaseSelfContainedAccessTokenClaimsCodec {

  public static final String ID4ME_IDENTIFIER_CLAIM = "id4me.identifier";

  private static final Logger LOG = LogManager.getLogger(AddIdentifierClaimCodec.class);
  private static final String CLIENT_ID_CLAIM = "client_id";
  private static final String SCOPE_CLAIM = "scope";
  private static final String CLAIM_CLAIM = "clm";

  @Override
  public JWTClaimsSet encode(final AccessTokenAuthorization accessTokenAuthz, final TokenEncoderContext tokenEncoderContext) {
    LOG.info("ENCODE access token authz: {}", accessTokenAuthz);
    final JWTClaimsSet.Builder builder = new JWTClaimsSet.Builder(super.encode(accessTokenAuthz, tokenEncoderContext));
    builder.claim(SCOPE_CLAIM, accessTokenAuthz.getScope());
    builder.claim(CLIENT_ID_CLAIM, accessTokenAuthz.getClientID());
    builder.claim(CLAIM_CLAIM, accessTokenAuthz.getClaimNames());
    final JSONObject accessTokenAuthzData = accessTokenAuthz.getData();
    if (accessTokenAuthzData.containsKey(ID4ME_IDENTIFIER_CLAIM)) {
      final String id4meIdentifier = accessTokenAuthzData.get(ID4ME_IDENTIFIER_CLAIM).toString();
      builder.claim(ID4ME_IDENTIFIER_CLAIM, id4meIdentifier);
    }
    final JWTClaimsSet result = builder.build();
    LOG.info("ENCODE result: {}", result);
    return result;
  }

  @Override
  public AccessTokenAuthorization decode(final JWTClaimsSet claimsSet, final TokenCodecContext tokenCodecContext) throws
          TokenDecodeException {
    final MutableAccessTokenAuthorization result = new MutableAccessTokenAuthorization(super.decode(claimsSet,
            tokenCodecContext));
    try {
      enrichWithRequiredClaims(result, claimsSet);
    } catch (ParseException e) {
      e.printStackTrace();
    }
    LOG.info("DECODE result: {}", result);
    return result;
  }

  /**
   * According to <a href="https://static.javadoc.io/com
   * .nimbusds/c2id-server-sdk/3.26.1/com/nimbusds/openid/connect/provider/spi/tokens
   * /BaseSelfContainedAccessTokenClaimsCodec.html">Javadoc</a> you have to do this!
   */
  private void enrichWithRequiredClaims(final MutableAccessTokenAuthorization result, final JWTClaimsSet claimsSet) throws ParseException {
    result.withClientID(new ClientID(claimsSet.getStringClaim(CLIENT_ID_CLAIM)));
    final List<String> scopeValues = claimsSet.getStringListClaim(SCOPE_CLAIM);
    scopeValues.stream().map(Scope::new).forEach(result::withScope);
    final List<String> claimClaim = claimsSet.getStringListClaim(CLAIM_CLAIM);
    result.withClaimNames(claimClaim == null ? emptySet() : new HashSet<>(claimClaim));
//      result.withClaimsLocales();
//      result.withPresetClaims();
    result.withData(domainIdAsJsonObject(claimsSet.getStringClaim(ID4ME_IDENTIFIER_CLAIM)));
  }

  private JSONObject domainIdAsJsonObject(final String identifierValue) {
    final Map<String, String> domainIdMap = new HashMap<>();
    domainIdMap.put(ID4ME_IDENTIFIER_CLAIM, identifierValue);
    return new JSONObject(domainIdMap);
  }

}

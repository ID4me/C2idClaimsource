/*^
  ===========================================================================
  ID4me Connect2ID ClaimSource
  ===========================================================================
  Copyright (C) 2017-2018 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.domainid.connect2id.claimsource;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
import com.nimbusds.jwt.JWTClaimsSet;
import com.nimbusds.jwt.JWTParser;
import com.nimbusds.langtag.LangTag;
import com.nimbusds.oauth2.sdk.id.Subject;
import com.nimbusds.oauth2.sdk.token.BearerAccessToken;
import com.nimbusds.openid.connect.provider.spi.InitContext;
import com.nimbusds.openid.connect.provider.spi.claims.AdvancedClaimsSource;
import com.nimbusds.openid.connect.provider.spi.claims.ClaimsSourceRequestContext;
import com.nimbusds.openid.connect.sdk.claims.DistributedClaims;
import com.nimbusds.openid.connect.sdk.claims.UserInfo;
import de.denic.domainid.connect2id.claimsource.entity.OpenIdConfigurationResponseEntity;
import de.denic.domainid.dns.DomainIdDnsClient;
import de.denic.domainid.dns.HostName;
import de.denic.domainid.dns.LookupResult;
import de.denic.domainid.dns.impl.DomainIdDnsClientDnsJavaBasedImpl;
import org.apache.commons.lang3.Validate;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;

import javax.servlet.ServletContext;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import java.io.IOException;
import java.net.URI;
import java.text.ParseException;
import java.time.Duration;
import java.time.Instant;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import static de.denic.domainid.connect2id.accesstokenclaimscodec.AddIdentifierClaimCodec.ID4ME_IDENTIFIER_CLAIM;
import static java.lang.String.format;
import static java.util.Arrays.asList;
import static java.util.Collections.singleton;
import static java.util.Collections.unmodifiableSet;
import static java.util.Optional.empty;
import static java.util.Optional.ofNullable;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON_TYPE;
import static org.apache.commons.lang3.RegExUtils.replacePattern;
import static org.apache.commons.lang3.StringUtils.EMPTY;
import static org.apache.commons.lang3.StringUtils.defaultIfEmpty;

/**
 * Hint: If renaming this type, update content of Java {@link java.util.ServiceLoader} config file appropriately!
 */
public final class Id4MeIdentityAgentClaimsSource implements AdvancedClaimsSource {

  private static final String ALL_ID4ME_CLAIMS = "id4me.*";
  private static final Set<String> SUPPORTED_BOOLEAN_CLAIMS = unmodifiableSet(new HashSet<>(asList("email_verified",
          "phone_number_verified"
  )));
  private static final Set<String> SUPPORTED_NUMERIC_CLAIMS = singleton("updated_at");
  private static final Set<String> SUPPORTED_JSON_OBJECT_CLAIMS = singleton("address");
  /**
   * According to
   * <a href="https://static.javadoc.io/com.nimbusds/c2id-server-sdk/3.16/com/nimbusds/openid/connect/provider/spi/claims/ClaimsSupport.html#supportedClaims">Javadoc</a>
   * it should NOT contain 'sub' claim!
   */
  private static final Set<String> SUPPORTED_STRING_CLAIMS = unmodifiableSet(
          new HashSet<>(asList(//"sub" see Javadoc above!
                  "name",
                  "given_name",
                  "family_name",
                  "middle_name",
                  "nickname",
                  "preferred_username",
                  "profile",
                  "picture",
                  "website",
                  "email",
                  "gender",
                  "birthdate",
                  "zoneinfo",
                  "locale",
                  "phone_number",
                  ALL_ID4ME_CLAIMS
          )));
  private static final Set<String> UNMODIFIABLE_SUPPORTED_CLAIMS;
  private static final ObjectReader GET_DATA_OF_SESSION_RESPONSE_ENTITY_READER = new ObjectMapper()
          .readerFor(OpenIdConfigurationResponseEntity.class);

  static {
    final Set<String> allSupportedClaims = new HashSet<>(SUPPORTED_STRING_CLAIMS);
    allSupportedClaims.addAll(SUPPORTED_BOOLEAN_CLAIMS);
    allSupportedClaims.addAll(SUPPORTED_NUMERIC_CLAIMS);
    allSupportedClaims.addAll(SUPPORTED_JSON_OBJECT_CLAIMS);
    UNMODIFIABLE_SUPPORTED_CLAIMS = unmodifiableSet(allSupportedClaims);
  }

  private final DomainIdDnsClient dnsClient;
  private final Client restClient;
  private volatile ServletContext context;

  /**
   * Required with {@link java.util.ServiceLoader} infrastructure!
   */
  public Id4MeIdentityAgentClaimsSource() {
    this(new DomainIdDnsClientDnsJavaBasedImpl(), ClientBuilder.newClient());
  }

  /**
   * Mainly for unit testing purposes (injecting mocks)
   *
   * @param dnsClient  Required
   * @param restClient Required
   */
  public Id4MeIdentityAgentClaimsSource(final DomainIdDnsClient dnsClient, final Client restClient) {
    Validate.notNull(dnsClient, "Missing DNS client");
    Validate.notNull(restClient, "Missing REST client");
    this.dnsClient = dnsClient;
    this.restClient = restClient;
  }

  @Override
  public void init(final InitContext initContext) {
    this.context = initContext.getServletContext();
  }

  @Override
  public boolean isEnabled() {
    return true;
  }

  @Override
  public void shutdown() {
    // Intentionally left empty
  }

  @Override
  public Set<String> supportedClaims() {
    return UNMODIFIABLE_SUPPORTED_CLAIMS;
  }

  @Override
  public UserInfo getClaims(final Subject subject, final Set<String> claimsRequested, final List<LangTag> claimsLocales, final ClaimsSourceRequestContext requestContext) {
    final BearerAccessToken userInfoAccessToken = (BearerAccessToken) requestContext.getUserInfoAccessToken();
    final String accessTokenJWTValue = userInfoAccessToken.toJSONObject().get("access_token").toString();
    final String identifier;
    try {
      final JWTClaimsSet jwtClaimsSet = JWTParser.parse(accessTokenJWTValue).getJWTClaimsSet();
      identifier = jwtClaimsSet.getStringClaim(ID4ME_IDENTIFIER_CLAIM);
    } catch (ParseException e) {
      throw new RuntimeException("Parsing access token JWT failed: " + accessTokenJWTValue, e);
    }

    final Optional<OpenIdConfigurationResponseEntity> optOpenIdConfiguration = getOpenIdConfigurationOfIdentityAgentOf(identifier);
    final UserInfo userInfo = new UserInfo(subject);
    if (optOpenIdConfiguration.isPresent()) {
      final OpenIdConfigurationResponseEntity openIdConfiguration = optOpenIdConfiguration.get();
      final DistributedClaims distributedClaims = new DistributedClaims(claimsRequested, openIdConfiguration.getUserInfoEndpointURL(),
              userInfoAccessToken);
      userInfo.addDistributedClaims(distributedClaims);
    } else {
      context.log(format("SKIPPED addition of distributed claims for '%1$s' cause no OpenID configuration available",
              identifier));
    }
    return userInfo;
  }

  /**
   * @param identifier Required
   * @return Never <code>null</code>
   */
  private Optional<OpenIdConfigurationResponseEntity> getOpenIdConfigurationOfIdentityAgentOf(final String identifier) {
    Validate.notEmpty(identifier, "Missing identifier");
    final LookupResult<HostName> lookupOfIdentAgentHostName = dnsClient.lookupIdentityAgentHostNameOf(identifier);
    return lookupOfIdentAgentHostName.isAvailable() ? ofNullable(queryForOpenIdConfiguration(lookupOfIdentAgentHostName.get())) : empty();
  }

  /**
   * @param hostName Required
   * @return Never <code>null</code>
   */
  private OpenIdConfigurationResponseEntity queryForOpenIdConfiguration(final HostName hostName) {
    Validate.notNull(hostName, "Missing hostname");
    context.log(format("Querying '%1$s' for OpenID Configuration", hostName));
    final WebTarget target = restClient
            .target(UriBuilder.fromUri("https://{host-name}/.well-known/openid-configuration").build(hostName.getValue()));
    final Pair<Response, String> openIdConfigurationResponse = invoke(target.request(APPLICATION_JSON_TYPE).buildGet(),
            target.getUri(), "GET", EMPTY);
    final Response response = openIdConfigurationResponse.getLeft();
    try {
      switch (response.getStatus()) {
        case 200:
          final OpenIdConfigurationResponseEntity responseEntity =
                  GET_DATA_OF_SESSION_RESPONSE_ENTITY_READER
                          .readValue(openIdConfigurationResponse.getRight());
          context.log(format("OpenID Configuration of '%1$s': %2$s", hostName, responseEntity));
          return responseEntity;
        default:
          return null;
      }
    } catch (final IOException e) {
      throw new RuntimeException("Internal error unmarshalling " + openIdConfigurationResponse.getRight(), e);
    } finally {
      response.close();
    }
  }

  /**
   * @param invocation                        Required
   * @param targetUriForLoggingOnly           Required, for logging purposes only
   * @param httpMethodForLoggingOnly          Required, for logging purposes only
   * @param optionalRequestBodyForLoggingOnly Optional, for logging purposes only
   * @return Never <code>null</code>, contains {@link Response} instance as well as response's body as {@link String}.
   */
  private Pair<Response, String> invoke(final Invocation invocation, final URI targetUriForLoggingOnly,
                                        final String httpMethodForLoggingOnly, final String optionalRequestBodyForLoggingOnly) {
    Validate.notNull(invocation, "Missing invocation");
    Validate.notNull(httpMethodForLoggingOnly, "Missing HTTP method");
    Validate.notNull(targetUriForLoggingOnly, "Missing target URI");
    final String requestBody = defaultIfEmpty(optionalRequestBodyForLoggingOnly, EMPTY);
    context.log(format("==> %1$s: >%2$s< (%3$s)", httpMethodForLoggingOnly, requestBody, targetUriForLoggingOnly));
    final Instant startingAt = Instant.now();
    final Response invocationResponse = invocation.invoke();
    // {
    // final SSLSessionContext serverSessionContext = restApiClient.getSslContext().getServerSessionContext();
    // final Enumeration<byte[]> sslSessionIDs = serverSessionContext.getIds();
    // System.out.println("===> Session IDs: " + sslSessionIDs);
    // try {
    // while (sslSessionIDs.hasMoreElements()) {
    // final byte[] sslSessionID = sslSessionIDs.nextElement();
    // System.out.println("===> Session ID: " + sslSessionID);
    // final SSLSession sslSession = serverSessionContext.getSession(sslSessionID);
    // X509Certificate[] peerCertificateChain;
    // peerCertificateChain = sslSession.getPeerCertificateChain();
    // for (int i = 0; i < peerCertificateChain.length; i++) {
    // final X509Certificate x509Certificate = peerCertificateChain[i];
    // System.out.println("===> Checking validity of: " + x509Certificate);
    // x509Certificate.checkValidity();
    // }
    // }
    // } catch (final SSLPeerUnverifiedException | CertificateExpiredException | CertificateNotYetValidException e) {
    // e.printStackTrace();
    // }
    // }
    final Duration invocationDuration = Duration.between(startingAt, Instant.now());
    final String responseEntity = invocationResponse.readEntity(String.class);
    context.log(format("<== %1$s %2$s: >%3$s< (%4$s, elapsed %5$s)", invocationResponse.getStatus(),
            invocationResponse.getStatusInfo(), replacePattern(responseEntity, "\\r?\\n", "|>>"),
            targetUriForLoggingOnly, invocationDuration));
    return new ImmutablePair<>(invocationResponse, responseEntity);
  }

}

/*^
  ===========================================================================
  ID4me Connect2ID ClaimSource
  ===========================================================================
  Copyright (C) 2017-2018 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.domainid.connect2id.claimsource.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.apache.commons.lang3.Validate;

import java.net.URI;
import java.util.HashSet;
import java.util.Set;

import static java.util.Collections.emptySet;
import static java.util.Collections.unmodifiableSet;

@JsonIgnoreProperties(ignoreUnknown = true)
public class OpenIdConfigurationResponseEntity {

  private final URI userInfoEndpointURL;
  private final Set<String> supportedClaims;

  /**
   * @param userInfoEndpointURL Required
   * @param supportedClaims     Optional
   */
  public OpenIdConfigurationResponseEntity(@JsonProperty("userinfo_endpoint") final URI userInfoEndpointURL, @JsonProperty("claims_supported") final Set<String> supportedClaims) {
    Validate.notNull(userInfoEndpointURL, "Missing user info endpoint URL");
    this.userInfoEndpointURL = userInfoEndpointURL;
    this.supportedClaims = (supportedClaims == null ? emptySet() : unmodifiableSet(new HashSet<>(supportedClaims)));
  }

  /**
   * @return Never <code>null</code>
   */
  public URI getUserInfoEndpointURL() {
    return userInfoEndpointURL;
  }

  /**
   * @return Never <code>null</code> but unmodifiable.
   */
  public Set<String> getSupportedClaims() {
    return supportedClaims;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    OpenIdConfigurationResponseEntity that = (OpenIdConfigurationResponseEntity) o;
    if (userInfoEndpointURL != null ? !userInfoEndpointURL.equals(that.userInfoEndpointURL) : that.userInfoEndpointURL != null)
      return false;
    return supportedClaims != null ? supportedClaims.equals(that.supportedClaims) : that.supportedClaims == null;
  }

  @Override
  public int hashCode() {
    int result = userInfoEndpointURL != null ? userInfoEndpointURL.hashCode() : 0;
    result = 31 * result + (supportedClaims != null ? supportedClaims.hashCode() : 0);
    return result;
  }

  @Override
  public String toString() {
    return "userInfoEndpointURL=" + userInfoEndpointURL + ", supportedClaims=" + supportedClaims;
  }

}

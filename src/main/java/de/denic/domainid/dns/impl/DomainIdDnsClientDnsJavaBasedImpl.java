/*^
  ===========================================================================
  ID4me Connect2ID ClaimSource
  ===========================================================================
  Copyright (C) 2017-2018 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.domainid.dns.impl;

import de.denic.domainid.dns.DomainIdDnsClient;
import de.denic.domainid.dns.HostName;
import de.denic.domainid.dns.IdentityAgentLookupException;
import de.denic.domainid.dns.LookupResult;
import org.apache.commons.lang3.Validate;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.xbill.DNS.*;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.List;
import java.util.Objects;
import java.util.Properties;
import java.util.function.Function;
import java.util.function.Predicate;

import static de.denic.domainid.dns.LookupResult.empty;
import static java.net.IDN.toASCII;
import static java.net.IDN.toUnicode;
import static java.util.Arrays.stream;
import static java.util.Collections.emptyList;
import static org.apache.commons.lang3.ArrayUtils.isEmpty;
import static org.apache.commons.lang3.ObjectUtils.defaultIfNull;
import static org.apache.commons.lang3.StringUtils.defaultString;
import static org.apache.commons.lang3.StringUtils.join;
import static org.xbill.DNS.DClass.IN;
import static org.xbill.DNS.Flags.AD;
import static org.xbill.DNS.Flags.DO;
import static org.xbill.DNS.Message.newQuery;
import static org.xbill.DNS.Name.*;
import static org.xbill.DNS.Record.newRecord;
import static org.xbill.DNS.Section.ANSWER;
import static org.xbill.DNS.Type.TXT;

public final class DomainIdDnsClientDnsJavaBasedImpl implements DomainIdDnsClient {

  private static final Logger LOG = LogManager.getLogger(DomainIdDnsClientDnsJavaBasedImpl.class);
  private static final int EDNS_VERSION_ZERO = 0;
  private static final int PAYLOAD_4096 = 4096;
  @SuppressWarnings("rawtypes")
  private static final List NO_EDNS_OPTIONS = emptyList();
  private static final String IDENTITY_AGENT_TAG = "iag";
  private static final String CLAIMS_PROVIDER_TAG = "clp";
  private static final String VERSION_TAG = "v";
  private static final Name DOMAINID_SUBDOMAIN = fromConstantString("_domainid");
  private static final String DOMAINID_VERSION_TAG_VALUE = "DID1";
  private static final Name OPENID_SUBDOMAIN = fromConstantString("_openid");
  private static final String OPENID_VERSION_TAG_VALUE = "OID1";
  private static final Predicate<? super Record> IS_TXT_RECORD = (record -> record instanceof TXTRecord);
  private static final Function<? super Record, ? extends TXTRecord> CAST_TO_TXT_RECORD = (record -> (TXTRecord) record);

  private final Resolver resolver;
  private final String resolverDescription;

  /**
   * @param resolver Optional, if missing an instance of {@link SimpleResolver} gets applied.
   */
  public DomainIdDnsClientDnsJavaBasedImpl(final Resolver resolver) {
    try {
      this.resolver = defaultIfNull(resolver, new SimpleResolver());
    } catch (final UnknownHostException e) {
      throw new RuntimeException("Internal error setting up default DNS resolver", e);
    }

    this.resolver.setEDNS(EDNS_VERSION_ZERO, PAYLOAD_4096, DO, NO_EDNS_OPTIONS);
    this.resolverDescription = (this.resolver instanceof SimpleResolver ? ((SimpleResolver) this.resolver).getAddress()
            .toString() : this.resolver.toString());
  }

  /**
   * @see #DomainIdDnsClientDnsJavaBasedImpl(Resolver)
   */
  public DomainIdDnsClientDnsJavaBasedImpl() {
    this(null);
  }

  private static String getJoinedValueFromTXTRecord(final TXTRecord txtRecord) {
    Validate.notNull(txtRecord, "Missing TXT record to parse");
    return join(txtRecord.getStrings(), "");
  }

  @Override
  public LookupResult<HostName> lookupIdentityAgentHostNameOf(final String identifier) throws IdentityAgentLookupException {
    Validate.notEmpty(identifier, "Missing identifier");
    final LookupResult<HostName> openIdSubdomainLookup = doLookupApplying(identifier, OPENID_SUBDOMAIN, OPENID_VERSION_TAG_VALUE);
    if (openIdSubdomainLookup.isEmpty()) {
      return doLookupApplying(identifier, DOMAINID_SUBDOMAIN, DOMAINID_VERSION_TAG_VALUE);
    }

    return openIdSubdomainLookup;
  }

  /**
   * @param identifier              Required
   * @param subdomain               Required
   * @param expectedVersionTagValue Required
   * @return Never <code>null</code>
   */
  private LookupResult<HostName> doLookupApplying(final String identifier, final Name subdomain,
                                                  final String expectedVersionTagValue) {
    Validate.notNull(subdomain, "Missing subdomain");
    final Name domainToLookup;
    final Record[] answerSectionsRecords;
    final boolean validatedResponse;
    try {
      domainToLookup = concatenate(subdomain, fromString(toASCII(identifier), root));
      LOG.info("Looking up TXT RRs of '{}' (resolver {})", domainToLookup, resolverDescription);
      final Message txtRRsOfDomainQuery = newQuery(newRecord(domainToLookup, TXT, IN));
      final Message queryResponse = resolver.send(txtRRsOfDomainQuery);
      validatedResponse = checkForAuthenticatedResponse(queryResponse, TXT, domainToLookup);
      answerSectionsRecords = queryResponse.getSectionArray(ANSWER);
    } catch (final IOException e) {
      throw new IdentityAgentLookupException(identifier, e);
    }

    if (answerSectionsRecords == null || isEmpty(answerSectionsRecords)) {
      LOG.debug("Lookup yields no TXT RR with '{}'", domainToLookup);
      return empty(validatedResponse);
    }

    return new LookupResult.Impl<>(validatedResponse,
            stream(answerSectionsRecords).filter(IS_TXT_RECORD).map(CAST_TO_TXT_RECORD)
                    .map(txtRecord -> parseIdentityAgentHostNameFrom(txtRecord, expectedVersionTagValue)).filter((Objects::nonNull))
                    .map(HostName::new).findAny());
  }

  /**
   * @param txtRecord               Required
   * @param expectedVersionTagValue Required
   * @return Optional
   */
  private String parseIdentityAgentHostNameFrom(final TXTRecord txtRecord, final String expectedVersionTagValue) {
    Validate.notEmpty(expectedVersionTagValue, "Missing expected version tag value");
    final String joinedDomainIdTxtRecordContent = getJoinedValueFromTXTRecord(txtRecord);
    final Properties txtRecordAsProperties = new Properties();
    stream(joinedDomainIdTxtRecordContent.split(";")).map(keyValueString -> keyValueString.split("="))
            .forEach(keyValueArray -> {
              if (keyValueArray.length > 1) {
                txtRecordAsProperties.put(keyValueArray[0].trim(), keyValueArray[1].trim());
              }
            });
    final String domainIdVersionTagValue = txtRecordAsProperties.getProperty(VERSION_TAG);
    if (!expectedVersionTagValue.equals(domainIdVersionTagValue)) {
      LOG.warn("Received domainID TXT record contains NO appropriate version tag: '{}'",
              defaultString(domainIdVersionTagValue, ""));
      return null;
    }

    final String result = defaultString(
            txtRecordAsProperties.getProperty(IDENTITY_AGENT_TAG),
            txtRecordAsProperties.getProperty(CLAIMS_PROVIDER_TAG));
    return (result == null ? null : toUnicode(result));
  }

  /*
   * TODO: Later on throw exception instead of simply logging the fact of unauthenticated DNS response!
   */
  private boolean checkForAuthenticatedResponse(final Message response, final int record, final Name domain) {
    final boolean validated = response.getHeader().getFlag(AD);
    if (!validated) {
      LOG.warn("CAUTION: Received unauthenticated DNS response asking for {} records of '{}' (respolver '{}')",
              Type.string(record), domain, resolverDescription);
    }
    return validated;
  }

}

/*^
  ===========================================================================
  ID4me Connect2ID ClaimSource
  ===========================================================================
  Copyright (C) 2017-2018 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.domainid.connect2id.claimsource;

import com.nimbusds.langtag.LangTag;
import com.nimbusds.oauth2.sdk.id.Subject;
import com.nimbusds.oauth2.sdk.token.AccessToken;
import com.nimbusds.oauth2.sdk.token.TypelessAccessToken;
import com.nimbusds.openid.connect.provider.spi.InitContext;
import com.nimbusds.openid.connect.provider.spi.claims.AdvancedClaimsSource;
import com.nimbusds.openid.connect.provider.spi.claims.ClaimsSourceRequestContext;
import com.nimbusds.openid.connect.sdk.claims.DistributedClaims;
import com.nimbusds.openid.connect.sdk.claims.UserInfo;
import de.denic.domainid.dns.impl.DomainIdDnsClientDnsJavaBasedImpl;
import de.denic.domainid.dns.util.AbstractUnitTestStartingLocalNameserver;
import de.denic.domainid.dns.util.LoggingResolverProxy;
import org.apache.commons.lang3.StringUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.xbill.DNS.SimpleResolver;

import javax.servlet.ServletContext;
import javax.ws.rs.client.ClientBuilder;
import java.lang.reflect.Proxy;
import java.net.URI;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static java.net.InetAddress.getLoopbackAddress;
import static java.util.Collections.emptyList;
import static org.easymock.EasyMock.*;
import static org.junit.Assert.*;

public class Id4MeIdentityAgentClaimsSourceIntegrationTest extends AbstractUnitTestStartingLocalNameserver {

  private static final List<LangTag> NO_CLAIM_LOCALES = emptyList();
  private static final String NAME_CLAIM = "name";
  private static final String EMAIL_VERIFIED_CLAIM = "email_verified";
  private static final AccessToken NO_CLAIM_ACCESS_TOKEN = null;
  private static final Class<?>[] SERVLET_CONTEXT_INTERFACE = new Class<?>[]{ServletContext.class};

  private AdvancedClaimsSource CUT;
  private InitContext initContextMock;
  private ClaimsSourceRequestContext requestContext;

  @Before
  public void setup() throws Exception {
    final SimpleResolver resolverTargettingJNamedProcess = new SimpleResolver();
    resolverTargettingJNamedProcess.setAddress(getLoopbackAddress());
    resolverTargettingJNamedProcess.setPort(35353);
    initContextMock = createMock(InitContext.class);
    requestContext = createMock(ClaimsSourceRequestContext.class);
    final ServletContext servletContextDummy = newServletContextDummy();
    CUT = new Id4MeIdentityAgentClaimsSource(
            new DomainIdDnsClientDnsJavaBasedImpl(new LoggingResolverProxy(resolverTargettingJNamedProcess)),
            ClientBuilder.newClient());
    expect(initContextMock.getServletContext()).andReturn(servletContextDummy);
    replay(initContextMock);
    CUT.init(initContextMock);
    verify(initContextMock);
    reset(initContextMock);
  }

  @After
  public void tearDown() throws Exception {
    CUT.shutdown();
  }

  @Ignore
  @Test
  public void integrateWithLocalNameserverDirectingToFreedomIdConnect2Id() throws Exception {
    final Subject subject = new Subject("claim-source.test");
    final Set<String> requestedClaims = new HashSet<>(Arrays.asList(EMAIL_VERIFIED_CLAIM, NAME_CLAIM));
    AccessToken accessToken = new TypelessAccessToken("foo-access-token");
    expect(requestContext.getUserInfoAccessToken()).andReturn(accessToken);
    replay(initContextMock, requestContext);
    {
      final UserInfo userInfo = CUT.getClaims(subject, requestedClaims, NO_CLAIM_LOCALES, requestContext);
      verify(initContextMock, requestContext);
      assertNull("No name claim", userInfo.getName());
      assertNull("No email_verified claim", userInfo.getEmailVerified());
      assertNull("No email claim", userInfo.getEmailAddress());
      final Set<DistributedClaims> receivedDistrClaims = userInfo.getDistributedClaims();
      assertEquals("Single distributed claim", 1, receivedDistrClaims.size());
      final DistributedClaims expectedDistrClaim = new DistributedClaims(requestedClaims, URI.create("foo.bar"),
              NO_CLAIM_ACCESS_TOKEN);
      receivedDistrClaims.forEach(receivedClaim -> {
        assertTrue("Names of claims", receivedClaim.getNames().containsAll(expectedDistrClaim.getNames()));
        assertEquals("Access token", accessToken, receivedClaim.getAccessToken());
      });
    }
  }

  private ServletContext newServletContextDummy() {
    return (ServletContext) Proxy.newProxyInstance(getClass().getClassLoader(), SERVLET_CONTEXT_INTERFACE,
            (proxy, method, parameters) -> {
              if ("log".equals(method.getName())) {
                System.out.println(StringUtils.join(parameters));
                return null; // void
              }

              throw new UnsupportedOperationException("Method: " + method);
            });
  }

}

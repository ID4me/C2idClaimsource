/*^
  ===========================================================================
  ID4me Connect2ID ClaimSource
  ===========================================================================
  Copyright (C) 2017-2018 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.domainid.connect2id.claimsource;

import com.nimbusds.jose.JWSHeader;
import com.nimbusds.jose.JWSObject;
import com.nimbusds.jose.Payload;
import com.nimbusds.jose.crypto.MACSigner;
import com.nimbusds.langtag.LangTag;
import com.nimbusds.oauth2.sdk.id.Subject;
import com.nimbusds.oauth2.sdk.token.AccessToken;
import com.nimbusds.oauth2.sdk.token.BearerAccessToken;
import com.nimbusds.openid.connect.provider.spi.InitContext;
import com.nimbusds.openid.connect.provider.spi.claims.AdvancedClaimsSource;
import com.nimbusds.openid.connect.provider.spi.claims.ClaimsSourceRequestContext;
import com.nimbusds.openid.connect.sdk.claims.DistributedClaims;
import com.nimbusds.openid.connect.sdk.claims.UserInfo;
import de.denic.domainid.dns.DomainIdDnsClient;
import de.denic.domainid.dns.HostName;
import de.denic.domainid.dns.LookupResult;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import javax.servlet.ServletContext;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.Invocation.Builder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;
import java.net.URI;
import java.security.SecureRandom;
import java.util.HashSet;
import java.util.List;
import java.util.ServiceLoader;
import java.util.Set;

import static com.nimbusds.jose.JWSAlgorithm.HS256;
import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;
import static java.util.stream.StreamSupport.stream;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON_TYPE;
import static javax.ws.rs.core.Response.Status.NOT_FOUND;
import static javax.ws.rs.core.Response.Status.OK;
import static org.easymock.EasyMock.*;
import static org.junit.Assert.*;

public class Id4MeIdentityAgentClaimsSourceTest {

  private static final boolean NOT_PARALLEL = false;
  private static final List<LangTag> NO_CLAIM_LOCALES = emptyList();
  private static final String NAME_CLAIM = "name";
  private static final String EMAIL_VERIFIED_CLAIM = "email_verified";
  private static final AccessToken NO_CLAIM_ACCESS_TOKEN = null;

  private AdvancedClaimsSource CUT;
  private DomainIdDnsClient dnsClientMock;
  private Client restClientMock;
  private InitContext initContextMock;
  private ServletContext servletContextMock;
  private WebTarget webTargetMock;
  private Builder builderMock;
  private Invocation invocationMock;
  private Response responseMock;
  private ClaimsSourceRequestContext requestContext;

  @Before
  public void setup() throws Exception {
    dnsClientMock = createMock(DomainIdDnsClient.class);
    restClientMock = createMock(Client.class);
    initContextMock = createMock(InitContext.class);
    servletContextMock = createMock(ServletContext.class);
    webTargetMock = createMock(WebTarget.class);
    builderMock = createMock(Builder.class);
    invocationMock = createMock(Invocation.class);
    responseMock = createMock(Response.class);
    requestContext = createMock(ClaimsSourceRequestContext.class);
    CUT = new Id4MeIdentityAgentClaimsSource(dnsClientMock, restClientMock);
    expect(initContextMock.getServletContext()).andReturn(servletContextMock);
    replay(initContextMock);
    CUT.init(initContextMock);
    verify(initContextMock);
    reset(initContextMock);
  }

  @After
  public void tearDown() throws Exception {
    CUT.shutdown();
  }

  @Test
  public void testGetDummyClaimsSuccessfully() throws Exception {
    {
      assertTrue("Enabled?", CUT.isEnabled());
    }
    {
      assertFalse("Any supported claims?", CUT.supportedClaims().isEmpty());
      assertTrue("Claim " + EMAIL_VERIFIED_CLAIM + " supported?", CUT.supportedClaims().contains(EMAIL_VERIFIED_CLAIM));
    }
    {
      final Subject subject = new Subject("pseudonym-pseudonym-pseudonym-pseudonym-pseudonym-test-domain-id");
      final Set<String> requestedClaims = new HashSet<>(asList(EMAIL_VERIFIED_CLAIM, NAME_CLAIM));
      expect(dnsClientMock.lookupIdentityAgentHostNameOf(isA(String.class)))
              .andReturn(new LookupResult.Impl<>(false, new HostName("dummy.uri.test")));
      final URI openIdConfigURI = URI.create("https://dummy.uri.test/.well-known/openid-configuration");
      expect(restClientMock.target(openIdConfigURI)).andReturn(webTargetMock);
      expect(webTargetMock.request(APPLICATION_JSON_TYPE)).andReturn(builderMock);
      expect(builderMock.buildGet()).andReturn(invocationMock);
      expect(webTargetMock.getUri()).andReturn(openIdConfigURI);
      servletContextMock.log(anyObject(String.class));
      expectLastCall().anyTimes();
      expect(invocationMock.invoke()).andReturn(responseMock);
      final URI userInfoEndpointURI = URI.create("http://dummy.url.test");
      expect(responseMock.readEntity(String.class))
              .andReturn("{\"userinfo_endpoint\":\"" + userInfoEndpointURI + "\", \"claims_supported\":[\"" +
                      EMAIL_VERIFIED_CLAIM + "\",\"" + NAME_CLAIM + "\"]}");
      expect(responseMock.getStatus()).andReturn(200).times(2);
      expect(responseMock.getStatusInfo()).andReturn(OK).times(1);
      //JWT
      final String jwtSerializedAccessToken = "{\"id4me.identifier\":\"identifier-value\"}";
      final JWSObject jwsObject = new JWSObject(new JWSHeader(HS256), new Payload(jwtSerializedAccessToken));
      byte[] sharedKey = new byte[32];
      new SecureRandom().nextBytes(sharedKey);
      jwsObject.sign(new MACSigner(sharedKey));
      final BearerAccessToken initialBearerAccessToken = new BearerAccessToken(jwsObject.serialize());
      expect(requestContext.getUserInfoAccessToken()).andReturn(initialBearerAccessToken);
      responseMock.close();
      replay(dnsClientMock, restClientMock, initContextMock, servletContextMock, webTargetMock, builderMock,
              invocationMock, responseMock, requestContext);
      {
        final UserInfo userInfo = CUT.getClaims(subject, requestedClaims, NO_CLAIM_LOCALES, requestContext);
        verify(dnsClientMock, restClientMock, initContextMock, servletContextMock, webTargetMock, builderMock,
                invocationMock, responseMock, requestContext);
        assertNull("No name claim", userInfo.getName());
        assertNull("No " + EMAIL_VERIFIED_CLAIM + " claim", userInfo.getEmailVerified());
        assertNull("No email claim", userInfo.getEmailAddress());
        final Set<DistributedClaims> receivedDistrClaims = userInfo.getDistributedClaims();
        assertEquals("Single distributed claim", 1, receivedDistrClaims.size());
        final DistributedClaims expectedDistrClaim = new DistributedClaims(requestedClaims, userInfoEndpointURI,
                NO_CLAIM_ACCESS_TOKEN);
        receivedDistrClaims.forEach(receivedDistrClaim -> {
          assertEquals("Source endpoint URL", expectedDistrClaim.getSourceEndpoint(),
                  receivedDistrClaim.getSourceEndpoint());
          assertTrue("Names of claims", receivedDistrClaim.getNames().containsAll(expectedDistrClaim.getNames()));
          assertEquals("Access token content", initialBearerAccessToken.getValue(), receivedDistrClaim.getAccessToken().getValue());
        });
      }
    }
  }

  @Test
  public void queryingForOpenIdConfigurationYieldsNOT_FOUND() throws Exception {
    {
      final Subject subject = new Subject("pseudonym-pseudonym-pseudonym-pseudonym-pseudonym-test-domain-id");
      final Set<String> requestedClaims = new HashSet<>(asList(EMAIL_VERIFIED_CLAIM, NAME_CLAIM));
      expect(dnsClientMock.lookupIdentityAgentHostNameOf(isA(String.class)))
              .andReturn(new LookupResult.Impl<>(false, new HostName("dummy.uri.test")));
      final URI openIdConfigURI = URI.create("https://dummy.uri.test/.well-known/openid-configuration");
      expect(restClientMock.target(openIdConfigURI)).andReturn(webTargetMock);
      expect(webTargetMock.request(APPLICATION_JSON_TYPE)).andReturn(builderMock);
      expect(builderMock.buildGet()).andReturn(invocationMock);
      expect(webTargetMock.getUri()).andReturn(openIdConfigURI);
      servletContextMock.log(anyObject(String.class));
      expectLastCall().anyTimes();
      expect(invocationMock.invoke()).andReturn(responseMock);
      expect(responseMock.readEntity(String.class)).andReturn("Plain text desribing NOT FOUND error.");
      expect(responseMock.getStatus()).andReturn(404).times(2);
      expect(responseMock.getStatusInfo()).andReturn(NOT_FOUND);
      //JWT
      final String jwtSerializedAccessToken = "{\"id4me.identifier\":\"identifier-value\"}";
      final JWSObject jwsObject = new JWSObject(new JWSHeader(HS256), new Payload(jwtSerializedAccessToken));
      byte[] sharedKey = new byte[32];
      new SecureRandom().nextBytes(sharedKey);
      jwsObject.sign(new MACSigner(sharedKey));
      final BearerAccessToken initialBearerAccessToken = new BearerAccessToken(jwsObject.serialize());
      expect(requestContext.getUserInfoAccessToken()).andReturn(initialBearerAccessToken);
      responseMock.close();
      replay(dnsClientMock, restClientMock, initContextMock, servletContextMock, webTargetMock, builderMock,
              invocationMock, responseMock, requestContext);
      {
        final UserInfo userInfo = CUT.getClaims(subject, requestedClaims, NO_CLAIM_LOCALES, requestContext);
        verify(dnsClientMock, restClientMock, initContextMock, servletContextMock, webTargetMock, builderMock,
                invocationMock, responseMock, requestContext);
        assertNull("No name claim", userInfo.getName());
        assertNull("No " + EMAIL_VERIFIED_CLAIM + " claim", userInfo.getEmailVerified());
        assertNull("No email claim", userInfo.getEmailAddress());
        final Set<DistributedClaims> receivedDistrClaims = userInfo.getDistributedClaims();
        assertNull("NO distributed claim", receivedDistrClaims);
      }
    }
  }

  @Test
  public void loadDummyClaimSourceViaServiceLoader() {
    final ServiceLoader<AdvancedClaimsSource> serviceLoader = ServiceLoader.load(AdvancedClaimsSource.class);
    assertTrue("DummyClaimSource loaded via ServiceLoader infrastructure", stream(serviceLoader.spliterator(),
            NOT_PARALLEL).anyMatch(service -> service instanceof Id4MeIdentityAgentClaimsSource));
  }
}

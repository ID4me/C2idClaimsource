/*^
  ===========================================================================
  ID4me Connect2ID ClaimSource
  ===========================================================================
  Copyright (C) 2017-2018 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.domainid.dns.impl;

import de.denic.domainid.dns.DomainIdDnsClient;
import de.denic.domainid.dns.HostName;
import de.denic.domainid.dns.LookupResult;
import de.denic.domainid.dns.util.AbstractUnitTestStartingLocalNameserver;
import de.denic.domainid.dns.util.LoggingResolverProxy;
import org.junit.Before;
import org.junit.Test;
import org.xbill.DNS.SimpleResolver;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;

import static java.net.InetAddress.getLoopbackAddress;
import static org.junit.Assert.*;

public class DomainIdDnsClientDnsJavaBasedImplTest extends AbstractUnitTestStartingLocalNameserver {

  /**
   * Infos regarding usage of TLD "test": https://datatracker.ietf.org/doc/html/rfc6761#section-6.2
   */
  private static final String TEST_DOMAIN_ID = "domain-id.test";
  private static final String TEST_INTERNATIONALIZED_DOMAIN_ID = "d\u00f6main-id.test";
  private static final InetAddress IP_ADDRESS_OF_VALIDATING_NAMESERVER;

  static {
    try {
      IP_ADDRESS_OF_VALIDATING_NAMESERVER = InetAddress.getByName("8.8.8.8"); // NOPMD
    } catch (final UnknownHostException e) {
      throw new ExceptionInInitializerError(e);
    }
  }

  private DomainIdDnsClient CUT;

  @Before
  public void setup() throws IOException {
    final SimpleResolver resolverTargettingJNamedProcess = new SimpleResolver();
    resolverTargettingJNamedProcess.setAddress(getLoopbackAddress());
    resolverTargettingJNamedProcess.setPort(35353);
    CUT = new DomainIdDnsClientDnsJavaBasedImpl(resolverTargettingJNamedProcess);
  }

  @Test
  public void successfulLookupOfIdentityAgentHostName() {
    final LookupResult<HostName> optIdentityAgentHost = CUT.lookupIdentityAgentHostNameOf(TEST_DOMAIN_ID);
    assertFalse("Identity Agent available", optIdentityAgentHost.isEmpty());
    assertEquals("Host of the Identity Agent", "ident-agent.domain-id.test", optIdentityAgentHost.get().getValue());
    assertFalse("Response NOT validated", optIdentityAgentHost.isValidated());
  }

  @Test
  public void successfulLookupOfInternationalizedIdentityAgentHostName() {
    final LookupResult<HostName> optIdentityAgentHost = CUT
            .lookupIdentityAgentHostNameOf(TEST_INTERNATIONALIZED_DOMAIN_ID);
    assertFalse("Identity Agent available", optIdentityAgentHost.isEmpty());
    assertEquals("Host of the Identity Agent", "ident-agent.d\u00f6main-id.test", optIdentityAgentHost.get().getValue());
    assertFalse("Response NOT validated", optIdentityAgentHost.isValidated());
  }

  @Test
  public void lookupOfUnknownDomain() {
    final LookupResult<HostName> optIdentityAgentHost = CUT.lookupIdentityAgentHostNameOf("unknown.test");
    assertTrue("No Identity Agent available", optIdentityAgentHost.isEmpty());
    assertFalse("Response NOT validated", optIdentityAgentHost.isValidated());
  }

  @Test
  public void lookupOfDomainMissingDomainId() {
    final LookupResult<HostName> optIdentityAgentHost = CUT
            .lookupIdentityAgentHostNameOf("missing-domain-id.test");
    assertTrue("No Identity Agent available", optIdentityAgentHost.isEmpty());
    assertFalse("Response NOT validated", optIdentityAgentHost.isValidated());
  }

  @Test
  public void lookupOfDomainIdMissingVersion_Tag() {
    final LookupResult<HostName> optIdentityAgentHost = CUT
            .lookupIdentityAgentHostNameOf("missing-v-tag-domain-id.test");
    assertTrue("No Identity Agent available", optIdentityAgentHost.isEmpty());
    assertFalse("Response NOT validated", optIdentityAgentHost.isValidated());
  }

  @Test
  public void lookupOfDomainIdWithUnrecognizedVersion_TagValue() {
    final LookupResult<HostName> optIdentityAgentHost = CUT
            .lookupIdentityAgentHostNameOf("unrecognized-v-tag-domain-id.test");
    assertTrue("No Identity Agent available", optIdentityAgentHost.isEmpty());
    assertFalse("Response NOT validated", optIdentityAgentHost.isValidated());
  }

  @Test
  public void lookupOfDomainIdWithMalformedTXT_RR_IagTagWithoutValue() {
    final LookupResult<HostName> optIdentityAgentHost = CUT
            .lookupIdentityAgentHostNameOf("malformed-domain-id.test");
    assertTrue("No Identity Agent available", optIdentityAgentHost.isEmpty());
    assertFalse("Response NOT validated", optIdentityAgentHost.isValidated());
  }

  @Test
  public void queryValidatingResolvingNameServers() throws UnknownHostException {
    final SimpleResolver resolverTargettingDenicValidatingNameServer = new SimpleResolver();
    resolverTargettingDenicValidatingNameServer.setAddress(IP_ADDRESS_OF_VALIDATING_NAMESERVER);
    resolverTargettingDenicValidatingNameServer.setPort(53);
    CUT = new DomainIdDnsClientDnsJavaBasedImpl(new LoggingResolverProxy(resolverTargettingDenicValidatingNameServer));
    {
      final LookupResult<HostName> optIdentityAgentHost = CUT.lookupIdentityAgentHostNameOf(TEST_DOMAIN_ID);
      assertTrue("No Identity Agent available", optIdentityAgentHost.isEmpty());
      assertTrue("Validated response", optIdentityAgentHost.isValidated());
    }
  }

}

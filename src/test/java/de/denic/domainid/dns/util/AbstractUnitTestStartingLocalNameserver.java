/*^
  ===========================================================================
  ID4me Connect2ID ClaimSource
  ===========================================================================
  Copyright (C) 2017-2018 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.domainid.dns.util;

import static java.util.Arrays.stream;
import static java.util.concurrent.TimeUnit.SECONDS;
import static org.apache.commons.lang3.StringUtils.join;
import static org.apache.commons.lang3.SystemUtils.JAVA_CLASS_PATH;
import static org.apache.commons.lang3.SystemUtils.USER_DIR;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Optional;

import org.junit.AfterClass;
import org.junit.BeforeClass;

/**
 * @version $Revision: 90152 $ ($Date: 2017-04-18 11:01:58 +0200 (Di, 18. Apr 2017) $) by $Author: christian $
 */
public abstract class AbstractUnitTestStartingLocalNameserver {

  protected static final String NAME_OF_LOCAL_HOST;
  static {
    try {
      NAME_OF_LOCAL_HOST = InetAddress.getLocalHost().getCanonicalHostName();
    } catch (final UnknownHostException e) {
      throw new ExceptionInInitializerError(e);
    }
  }

  private static Process jnamedProcess;
  private static InputStreamReadingRunnable jnamedSysoutReader;
  // private static File jnamedCacheFile;

  /**
   * Starts up the jnamed name server in a separate process.
   */
  @BeforeClass
  public static void setUpBeforeClass() throws Exception {
    // createJNamedCacheFile();
    startJNamedProcess();
    startSysOutReadingThread();
    Thread.sleep(500L); // Waiting half a second letting jnamed process start up completely
  }

  private static void startSysOutReadingThread() {
    jnamedSysoutReader = new InputStreamReadingRunnable(jnamedProcess.getInputStream());
    final Thread systemOutReadingThread = new Thread(jnamedSysoutReader, "jnamed sys.out reader");
    systemOutReadingThread.setDaemon(true);
    systemOutReadingThread.start();
  }

  /**
   */
  private static void startJNamedProcess() throws IOException {
    final Optional<Path> dnsjavaLibInClassPath = stream(JAVA_CLASS_PATH.split(File.pathSeparator))
        .map(pathName -> Paths.get(pathName)).filter(path -> {
          final String fileName = path.getFileName().toString();
          return fileName.startsWith("dnsjava-") && fileName.endsWith(".jar");
        }).findFirst();
    assertTrue("PRECONDITION: dnsjava JAR file available in class path", dnsjavaLibInClassPath.isPresent());
    final Path dnsjavaLibFile = dnsjavaLibInClassPath.get();
    final ProcessBuilder processBuilder = new ProcessBuilder("java", "-cp", dnsjavaLibFile.toString(), "jnamed",
        "./src/test/resources/jnamed/jnamed.conf").redirectErrorStream(true).directory(new File(USER_DIR));
    System.out.println("!!! Launching jnamed process: " + USER_DIR + "$ " + join(processBuilder.command(), ' '));
    jnamedProcess = processBuilder.start();
  }

  // private static void createJNamedCacheFile() throws IOException {
  // jnamedCacheFile = new File("./build/tmp/jnamed/cache.db").getCanonicalFile();
  // createDirectories(jnamedCacheFile.toPath());
  // if (!jnamedCacheFile.isFile()) {
  // assertTrue("PRECONDITION: Create jnamed cache file", jnamedCacheFile.createNewFile());
  // }
  // System.out.println("!!! Created jnamed cache file: " + jnamedCacheFile);
  // }

  @AfterClass
  public static void tearDownAfterClass() throws Exception {
    if (jnamedProcess != null) {
      jnamedProcess.destroy();
      jnamedProcess.waitFor(3L, SECONDS);
      System.out.println("!!! Stopping jnamed process yields exit code " + jnamedProcess.exitValue());
    }
    // if (jnamedCacheFile != null) {
    // jnamedCacheFile.deleteOnExit();
    // System.out.println("!!! Deleting jnamed cache file " + jnamedCacheFile + " on exit");
    // }
  }

}

/*^
  ===========================================================================
  ID4me Connect2ID ClaimSource
  ===========================================================================
  Copyright (C) 2017-2018 DENIC eG, 60329 Frankfurt am Main, Germany
  ===========================================================================
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
  ===========================================================================
*/

package de.denic.domainid.dns.util;

import java.io.IOException;
import java.util.List;

import org.apache.commons.lang3.NotImplementedException;
import org.apache.commons.lang3.Validate;
import org.xbill.DNS.Message;
import org.xbill.DNS.Resolver;
import org.xbill.DNS.ResolverListener;
import org.xbill.DNS.TSIG;

public final class LoggingResolverProxy implements Resolver {

  private final Resolver proxiedResolver;

  /**
   * @param resolverToProxy
   *          Required
   */
  public LoggingResolverProxy(final Resolver resolverToProxy) {
    Validate.notNull(resolverToProxy, "Missing resolver to proxy");
    this.proxiedResolver = resolverToProxy;
  }

  @Override
  public Message send(final Message message) throws IOException {
    System.out.println("==> " + message);
    final Message response = proxiedResolver.send(message);
    System.out.println("<== " + response);
    return response;
  }

  @Override
  public Object sendAsync(final Message message, final ResolverListener listener) {
    throw new NotImplementedException("");
  }

  @Override
  public void setEDNS(final int arg0) {
    throw new NotImplementedException("");
  }

  @Override
  public void setEDNS(final int level, final int payloadSize, final int flags,
      @SuppressWarnings("rawtypes") final List options) {
    proxiedResolver.setEDNS(level, payloadSize, flags, options);
  }

  @Override
  public void setIgnoreTruncation(final boolean arg0) {
    throw new NotImplementedException("");
  }

  @Override
  public void setPort(final int arg0) {
    throw new NotImplementedException("");
  }

  @Override
  public void setTCP(final boolean arg0) {
    throw new NotImplementedException("");
  }

  @Override
  public void setTSIGKey(final TSIG arg0) {
    throw new NotImplementedException("");
  }

  @Override
  public void setTimeout(final int arg0) {
    throw new NotImplementedException("");
  }

  @Override
  public void setTimeout(final int arg0, final int arg1) {
    throw new NotImplementedException("");
  }

  @Override
  public String toString() {
    return "Logging " + proxiedResolver;
  }

}
